Yii 2 Voodoo-Mobile
===================

Installation
------------
The preferred way to install this extensions is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require "voodoo-mobile/yii2vm" "*"
```
or add

```json
"voodoo-mobile/yii2vm" : "*"
```

to the require section of your application's `composer.json` file.
